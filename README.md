moved to https://git.grassecon.net/chaintool/chainlib-eth

# chainlib-eth

Ethereum implementation of the chainlib blockchain interface tooling

See https://gitlab.com/chaintool/chainlib for more information
